# =*= coding:utf-8 =*=
# かしら

import sys
import re

SPLITTER = re.compile(r'[\{\}\*\[\],\\|\s]+')

for line in sys.stdin :
	if "=" not in line :
		continue
	eqIndex = line.index('=')
	value   = line[eqIndex+1:]
	value   = value.replace('"','').replace("'","").strip()
	values  = SPLITTER.split(value)
	
	for v in values :
		if v :
			print(v.strip())


