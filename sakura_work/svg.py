# =*= coding:utf-8 =*=
# かしら

import bs4
import re

RE_REMOVE_CRLF  = re.compile(r"[\r\n]+")
RE_ADJUST_SPACE = re.compile(r"\s\s+")

def loadSVGReference() :
	with open("svg_single-page.html", "r", encoding="utf-8") as h :
		return h.read()
svgRef = loadSVGReference()
bs     = bs4.BeautifulSoup(svgRef, "html.parser")

def cleanString(s) :
	s = RE_REMOVE_CRLF.sub('',   s)
	s = RE_ADJUST_SPACE.sub(' ', s)
	return s.strip()

def adefListToStringList(adef) :
	dtList = adef.find_all("dt")
	r      = (cleanString("".join(dt.strings)) for dt in dtList)
	return r


adefList = bs.find_all("div", class_="adef-list")
defList  = ( adefListToStringList(adef) for adef in adefList )
#defList  = [ s for sub in defList for s in sub ]
defList  = "\n".join( s for sub in defList for s in sub )

print(defList)



